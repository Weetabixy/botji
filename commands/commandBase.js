var Discord = require("discord.js")
module.exports = {
    command: class Command {
        /**
         * @param {string} callCommand 
         * @param {string|null} configName
         * @param {Function} returnVal 
         */
        constructor(callCommand, configName, response) {

            this.name = callCommand.toLowerCase()

            this.responseCode = response

            this.config = configName
            this.enabled = true
        }
    },
    /**
     * 
     * @param {string} message What is said in the description
     * @param {string} blame Who to blame
     */
    error: function(message,blame){
        return new Discord.RichEmbed()
            .setTitle("An error has occured")
            .setDescription(message)
            .setColor("ff0000")
            .setFooter("Blame " + blame)
    }
}
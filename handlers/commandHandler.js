var fs = require("fs");
var config = require("./../config/config");
var commands = [];

fs.readdirSync(__dirname + "/../commands").forEach(file => {
    if (file == "commandBase.js") {
        return;
    } else {
        console.log("Command " + file + " loading")
        try{
            commands.push(require(__dirname + "/../commands/" + file));
        } catch{
            console.log(file + " could not be loaded ")
        }
        
    }


})
module.exports = {
    commands: commands,
    handleCommand: function handleCommand(msg,client) {

        commands.forEach(command => {
            if (msg.content.substring(1, msg.content.length).toLowerCase().includes(command.name)) {
                
                if (command.enabled) {

                    if (command.config != null) {                     
                        msg.channel.send(command.responseCode(msg,client,config[command.config]));
                    } else {
                        msg.channel.send(command.responseCode(msg,client));
                    }

                } else {
                    msg.channel.send(":no_entry_sign:  That command is disabled :no_entry_sign:")
                }

            }
        })
    },

    disableCommand: function disableCommand(commandName) {
        commands.forEach(command => {
            if (command.name == commandName) {
                command.enabled = false
            }
        });
    }

}


var Command = require("./commandBase")
var sReq = require("sync-request");
var Discord = require("discord.js");
module.exports = new Command.command("darksky", "darkskyToken", function(msg,clientparameter){
    var args = msg.content.split(' ')
    args.shift()
    if(args.length < 1){
        return new Command.error("You need more than 0 arguments", "yourself")
    } else{
        var pos
        // maybe ternattwdgrtdgert thing could be used but dont know that
        if(args.length == 1 ){
            pos = sReq("GET", 'https://darksky.net/geo?q='+args[0])
            pos = JSON.parse(pos.getBody())
        } else{
            pos = sReq("GET", 'https://darksky.net/geo?q='+args[0] + "," + args[1])
            pos = JSON.parse(pos.getBody())
        }
        console.log(pos)
        var req = sReq("GET", `https://api.darksky.net/forecast/${parameter}/${pos.latitude},${pos.longitude}?units=uk2`)
        switch(req.statusCode){
            case 403:
                return new Command.error("An incorect API key was used", "the admin")
                break
            case 400:
                return new Command.error("An incorrect place has been provided", "yourself")
        } 

        var res = JSON.parse(req.getBody())
        var icon;
        switch(res.currently.icon){
            case "clear-day":
                icon = "https://www.iconfinder.com/icons/3859136/download/png/128"
                bresk
            case "clear-night":
                icon = "https://www.iconfinder.com/icons/3859141/download/png/128"
                break
            case "rain":
                icon = "https://www.iconfinder.com/icons/3859144/download/png/128"
                break
            case "snow" || "sleet":
                icon = "https://www.iconfinder.com/icons/3859134/download/png/128"
                break
            case "wind":
                icon = "https://www.iconfinder.com/icons/3859154/download/png/128"
                break
            case "fog" || "cloudy":
                icon = "https://www.iconfinder.com/icons/3859132/download/png/128"
                break
            case "partly-cloudy-day":
                icon = "https://www.iconfinder.com/icons/3859147/download/png/128"
                break;
            case "partly-cloudy-night":
                icon = "https://www.iconfinder.com/icons/3859140/download/png/128"
                break           
        }

        return new Discord.RichEmbed()
            .setAuthor("Darksky","https://darksky.net/images/darkskylogo.png")
            .setTitle("Weather")
            .setDescription("The weather at " + res.latitude + ", " + res.longitude)
            .setColor("#00ff00")
            .setThumbnail(icon)
            .addField("Temperature", res.currently.temperature + "°C",true)
            .addField("Precipitation Probability", res.currently.precipProbability, true)
            .addField("Nearest Storm Bearing", res.currently.nearestStormBearing ? res.currently.nearestStormBearing + "°":"None(?)",true)
            .addField("Nearest Storm Distance",res.currently.nearestStormDistance+" Miles",true);
        
    }
    
    


})

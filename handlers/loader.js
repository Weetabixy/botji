const fs = require("fs");
var commandHandler = require("./commandHandler")
if (fs.existsSync(__dirname + "/../config/config.js")) {
    var config = require(__dirname + "/../config/config.js");
    // Get Token
    if (config.token == "") {

        if (process.argv[2] != undefined) {
            config.token = process.argv[2];
            console.clear()

        } else {
            if (process.env.TOKEN != undefined) {
                config.token - process.env.TOKEN;
            } else {
                console.log("no token anywhere time to go commit delete system32 of program");
                process.exit(420);
            }

        }
    }

    commandHandler.commands.forEach(command => {
        if (command.config != null) {
            if (config[command.config] == undefined || config[command.config] == "") {
                console.log(command.name + " has been disabled due to missing config")
                commandHandler.disableCommand(command.name)
            }
        }
    });

} else {
    console.log("Config not found :( time to go do self-murder");
    process.kill(69);
}

module.exports = {
    config: config,
    commandHandler: commandHandler
};

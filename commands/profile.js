var Discord = require("discord.js")

var Command = require("./commandBase")

var customStatusEmoji = {
    comOnline: "<:compOnline:628321056863813633>",
    comIdle: "<:compIdle:628321056649642019>",
    comDnd: "<:compDnd:628321056557367298>",
    mobOnline: "<:mobOnline:628326792373207073>",
    mobIdle: "<:mobIdle:628326792263892994>",
    mobDnd: "<:mobDnd:628326792507162624>",
    webOnline: "<:webOnline:628327675185856513>",
    webIdle: "<:webIdle:628327674950975519>",
    webDnd: "<:webDnd:628327675144175616>",
    offline: "<:offline:628313741452378125>"
}


module.exports = new Command.command("profile", null, function(msg,client){
    var memb;
    if(msg.mentions.members.array().length == 0){
        memb = msg.member
    } else {
        memb = msg.mentions.members.array()[0]
    }
    
    var status = getUserStatus(memb)
    switch(memb.user.presence.game.type){
        case 0:
            var type = "Is playing"
            break;
        case 1:
            var type = "Is streaming"
            break
        case 2:
            var type = "Is listening to"
        case 3:
            var type = "Is watching"
        default:
            var type = "Is doing"
    }
    var embed = new Discord.RichEmbed()
        .setColor("00ff00")
        .setAuthor(memb.user.tag+ "'s Profile")
        .setTitle(getUserStatus(memb) + " " + memb.displayName) // please dont read that function
        .setThumbnail(memb.user.avatarURL)
        .setDescription(memb.user.bot ? "The user is a bot" : "The user isn't a bot")
        .addField("Discord Birthday :birthday:", memb.user.createdAt.getDate()+"-"+memb.user.createdAt.getMonth()+"-"+memb.user.createdAt.getFullYear())
        .addField(type, memb.user.presence.game.name)
        .addField("User ID", memb.id)
    return embed
})

function getUserStatus(user){
    var status = {}

    switch(user.presence.clientStatus.desktop){
        case "online":
            status["computer"] = customStatusEmoji.comOnline
            break
        case "idle":
            status["computer"] = customStatusEmoji.comIdle
            break
        case "dnd":
            status["computer"] = customStatusEmoji.comDnd
            break
        default:
            status["computer"] = null
            break
    }

    switch(user.presence.clientStatus.mobile){
        case "online":
            status["mobile"] = customStatusEmoji.mobOnline
            break
        case "idle":
            status["mobile"] = customStatusEmoji.mobIdle
            break
        case "dnd":
            status["mobile"] = customStatusEmoji.mobDnd
            break
        default:
            status["mobile"] = null
            break
    }

    switch(user.presence.clientStatus.web){
        case "online":
            status["web"] = customStatusEmoji.webOnline
            break
        case "idle":
            status["web"] = customStatusEmoji.webIdle
            break
        case "dnd":
            status["web"] = customStatusEmoji.webDnd
            break
        default:
            status["web"] = null
            break
    }
    // Prepare for pain
    return `${status.computer!=null ? status.computer: "" } ${status.web!=null ? status.web : "" } ${status.mobile!=null ? status.mobile : "" }`
}
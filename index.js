var bot = require("./handlers/loader");

var Discord = require("discord.js");
const client = new Discord.Client();

client.on("ready", function(){
    console.log("Bot is now ready");
    client.user.setPresence({game: {name: `on ${client.guilds.array().length} servers  `}})
})


client.on("message", function(msg){
    if(msg.content.charAt(0) == bot.config.prefix){
        bot.commandHandler.handleCommand(msg,client);
    }
})

client.login(bot.config.token)
var Command = require("./commandBase");
var Discord = require("discord.js") 
var sReq = require("sync-request")

module.exports = new Command.command("urban",null, function(msg,client){
    var args = msg.content.split(' ')
    args.shift()

    if(args.length < 1){
        return new Command.error("You need more than 0 arguments", "yourself")
    } else {
        var url = "http://api.urbandictionary.com/v0/define?term="
        for(arg in args){
            url += `${args[arg]}%20`
        }
        
        
        
        var req = sReq("GET", url)
        var res = JSON.parse(req.getBody())
        
        if(res.list[0]){
            console.log("uwu")
        } else{
            return new Command.error("An incorrect query was used", "yourself")
        }

        var def = res.list[0].definition.replace(/\[|\]/g, '')
        var example = res.list[0].example.replace(/\[|\]/g, '')

        

        return new Discord.RichEmbed()  
            .setTitle(res.list[0].word, res.list[0].permalink)
            .setAuthor("Urban Dictionary", "https://firebounty.com/image/635-urban-dictionary","https://urbandictionary.com")
            .addField("Definition", def)
            .addField("Example",example)
            .setColor("00ff00")

    }


})